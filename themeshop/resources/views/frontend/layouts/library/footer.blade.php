<footer class="site-footer site-section">
    <div class="container">
        <!-- Footer Links -->
        <div class="row">
            <div class="col-sm-2 col-md-2 left">
                <h4 class="footer-heading">{{ __('Follow us') }}</h4>
                <ul class="footer-nav footer-nav-social list-inline">
                    <li><a href="javascript:void(0)"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="javascript:void(0)"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="javascript:void(0)"><i class="fa fa-google-plus"></i></a></li>
                    <li><a href="javascript:void(0)"><i class="fa fa-dribbble"></i></a></li>
                    <li><a href="javascript:void(0)"><i class="fa fa-rss"></i></a></li>
                </ul>
            </div>
            <div class="col-sm-8 col-md-8 left">
                <h4 class="footer-heading">{{ date('Y') }} &copy; 
                <a href="{{ url('') }}">{{ __('Matthew Store') }}</a></h4>
                <ul class="footer-nav list-inline title-footer">
                    <li>
                        <span class="title-footer">{{ __('Provide best quality machine for you') }}</span>
                    </li>
                </ul>
            </div>
            <div class="col-sm-2 col-md-2 left translate">
                <h4 class="footer-heading"></h4>
                <ul class="footer-nav footer-nav-social list-inline">
                    @php
                        $language = App\Helpers\Helper::getCurrentLanguage();
                    @endphp
                    <li>
                        <a href="{{ route('change-language', ['language' => 'vn']) }}" title="Vietnam" class="vietnam {{ $language == 'vn' ? 'active' : '' }}">
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('change-language', ['language' => 'en']) }}" title="English" class="english {{ $language == 'en' ? 'active' : '' }}">
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="footer-quick-link">
                <div class="col-sm-3 col-md-3">
                    <ul>
                        <li><a href="#">Smart Phone</a></li>
                        <li><a href="#">Ipad</a></li>
                        <li><a href="#">MacBook</a></li>
                        <li><a href="#">Headphone</a></li>
                        <li><a href="#">Speaker</a></li>
                        <li><a href="#">Smart Speaker</a></li>
                        <li><a href="#">Smart Watch</a></li>
                        <li><a href="#">Readbook Machine</a></li>
                    </ul>
                </div>
                <div class="col-sm-3 col-md-3">
                    <ul>
                        <li><a href="#">About Us</a></li>
                        <li><a href="#">Contact</a></li>
                        <li><a href="#">Partner</a></li>
                        <li><a href="#">Service</a></li>
                        <li><a href="#">Policy</a></li>
                        <li><a href="#">Blog</a></li>
                    </ul>
                </div>
                <div class="col-sm-3 col-md-3">
                    <ul>
                        <li><a href="#">Apple</a></li>
                        <li><a href="#">LG</a></li>
                        <li><a href="#">Samsung</a></li>
                        <li><a href="#">HTC</a></li>
                        <li><a href="#">Google</a></li>
                        <li><a href="#">Xiaomi</a></li>
                        <li><a href="#">Sonny</a></li>
                        <li><a href="#">Howay</a></li>
                    </ul>
                </div>
                <div class="col-sm-3 col-md-3">
                    <ul>
                        <li><a href="#">New Machine</a></li>
                        <li><a href="#">Comming soon</a></li>
                        <li><a href="#">Hot Sales 2014</a></li>
                        <li><a href="#">Hot Sales 2015</a></li>
                        <li><a href="#">Hot Sales 2016</a></li>
                        <li><a href="#">Hot Sales 2017</a></li>
                        <li><a href="#">Hot Sales 2018</a></li>
                        <li><a href="#">Hot Sales 2019</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- END Footer Links -->
    </div>
</footer>
