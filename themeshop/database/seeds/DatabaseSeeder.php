<?php

use Illuminate\Database\Seeder;
use App\Eloquent\InforWebsite;
use App\Eloquent\User;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $linkCommunication = [
            'facebook'  => 'facebook.com',
            'google'    => 'google.com',
        ];
        $dataMainInforWebsite = [
            'title'     => 'Title Website',
            'slogan'    => 'Website Slogan',
            'logo'      => '',
            'link_communications' => json_encode($linkCommunication),
            'position'  => InforWebsite::POSITION_MAIN,
            'footer'    => '',
            'options'   => '',
        ];
        InforWebsite::create($dataMainInforWebsite);

        $dataUserAdmin = [
            'email'             =>  'admin@gmail.com',
            'display_name'      =>  'Admin',
            'password'          =>  bcrypt('admin123'),
            'phone'             =>  '0988333999',
            'address'           =>  'Ha Noi',
            'permission'        =>  User::PERMISSION_ADMIN,
            'avatar'            =>  '',
        ];

        User::create($dataUserAdmin);
    }
}
