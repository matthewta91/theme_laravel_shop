<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Translate with middleware
Route::group(['middleware' => 'localization'], function(){

    Route::get('/', 'Frontend\HomeController@index')->name('welcome');
    Route::get('/search', 'Frontend\HomeController@search')->name('frontend.search');
    Route::get('/search/json', 'Frontend\HomeController@jsonSearch');
    Route::get('change-language', 'Frontend\HomeController@changeLanguage')->name('change-language');

    Route::get('/redirect/{providerName}', 'Frontend\SocialAuthController@redirect')->name('auth.social');
    Route::get('/callback/{providerName}', 'Frontend\SocialAuthController@callback');

    Auth::routes();

    Route::group(['prefix' => 'admin', 'middleware' => 'isAdmin'], function () {
        Route::get('/', 'Admin\AdminController@index');
        Route::match(['get', 'post'], 'setup', 'Admin\AdminController@setup')->name('admin.setup');
        Route::get('json/getStaticstics', 'Admin\AdminController@getStaticstics');

        Route::resource('users', 'Admin\UserController');

        Route::resource('category', 'Admin\CategoryController');
        Route::get('category/{id}/create', 'Admin\CategoryController@subCreate')->name('category.subCreate');

        Route::resource('product', 'Admin\ProductController');

        Route::resource('comment', 'Admin\CommentController');

        Route::resource('rate', 'Admin\RateController');

        Route::resource('request', 'Admin\RequestProductController');

        Route::get('orders', 'Admin\OrderController@index')->name('admin.orders.index');
        Route::get('orders/{id}/detail', 'Admin\OrderController@orderDetail')->name('admin.orders.detail');
        Route::put('orders/{id}/detail', 'Admin\OrderController@changeStatus')->name('admin.orders.change');
    });

    Route::group(['middleware' => 'isLogin'], function(){
        // Make An Appointment
        Route::get('/preorder', 'Frontend\PreOrderController@index')
            ->name('frontend.preorder.index');
        Route::get('/preorder/json/getList', 'Frontend\PreOrderController@getJsonList');
        Route::delete('/preorder/{id}', 'Frontend\PreOrderController@destroyAnAppointment');

        Route::get('/preorder/{id}/edit', 'Frontend\PreOrderController@editAnAppointment');
        Route::put('/preorder/{id}/edit', 'Frontend\PreOrderController@updateAnAppointment')
            ->name('frontend.preorder.update');

        Route::get('/preorder/addnew', 'Frontend\PreOrderController@addNewAnAppointment')
            ->name('frontend.preorder.addnew');
        Route::post('/preorder/addnew', 'Frontend\PreOrderController@storeNewAnAppointment')
            ->name('frontend.preorder.store');
        Route::get('/preorder/json/searchProducts', 'Frontend\PreOrderController@jsonSeachProducts');

        // Mark Product
        Route::get('/mark-product', 'Frontend\HomeController@markProductIndex')
            ->name('frontend.mark-product.index');
        Route::delete('/mark-product/{id}', 'Frontend\HomeController@markProductDestroy')
            ->name('frontend.mark-product.destroy');

        Route::get('detail/add-to-box', 'Frontend\DetailProductsController@addToBox')->name('add_to_box');

        // Add comment
        Route::post('/comment/send/data', 'Frontend\DetailProductsController@addEditComment');
        Route::post('/comment/send/data/children', 'Frontend\DetailProductsController@addEditChildrenComment');
        Route::post('/comment/delete/children', 'Frontend\DetailProductsController@deleteChildrenComment');

        Route::post('/user/edit', 'Frontend\UserProfilesController@editUserInformations')
            ->name('frontend.user.edit.personal');
        Route::post('/user/edit/upload', 'Frontend\UserProfilesController@showUploadAvatar')
            ->name('show_upload_avatar');
        Route::post('/user/change-password', 'Frontend\UserProfilesController@userChangePassword')
            ->name('user_change_password');
        Route::get('/user/profile', 'Frontend\UserProfilesController@index')
            ->name('frontend.user.profiles');


        Route::get('/make-an-appointment', 'Frontend\RequestMakeAnAppointmentController@requestIndex')
            ->name('make-an-appointment.index');
        Route::get('/make-an-appointment/add-new', 'Frontend\RequestMakeAnAppointmentController@requestAddnew')
            ->name('make-an-appointment.addnew');
        Route::post('/make-an-appointment/add-new', 'Frontend\RequestMakeAnAppointmentController@requestStore')
            ->name('make-an-appointment.store');
        Route::get('/make-an-appointment/json/detail', 'Frontend\RequestMakeAnAppointmentController@jsonDetail');
        Route::get('/make-an-appointment/json/partnerDetail', 'Frontend\RequestMakeAnAppointmentController@jsonPartnerDetail');

        Route::get('/request-product', 'Frontend\RequestProductController@requestIndex')
            ->name('frontend.request-product.index');
        Route::get('/request-product/add-new', 'Frontend\RequestProductController@requestAddnew')
            ->name('frontend.request-product.addnew');
        Route::post('/request-product/add-new', 'Frontend\RequestProductController@requestStore')
            ->name('frontend.request-product.store');
        Route::get('/request-product/json/detail', 'Frontend\RequestProductController@jsonDetail');

        Route::get('/partner-make-an-appointment', 'Frontend\RequestMakeAnAppointmentController@partnerRequestIndex')
            ->name('partner-request-an-appointment.index');
        Route::get('/partner/{id}/preorder', 'Frontend\PreOrderController@partnerMakeAnAppointment')
            ->name('frontend.partner.preorder');
        Route::post('/partner/{id}/preorder', 'Frontend\PreOrderController@partnerStoreAnAppointment')
            ->name('frontend.partner.preorder.store');

        Route::get('/convert-set-product/{id}/order', 'Frontend\OrderController@convertToOrder')
            ->name('frontend.convert.order');
        Route::get('checkout', 'Frontend\OrderController@checkout')->name('frontend.checkout');
        Route::post('checkout', 'Frontend\OrderController@checkoutStore')->name('frontend.checkout.store');
        Route::get('order-view/{id}/detail', 'Frontend\OrderController@detailOrder')
            ->name('frontend.order.detail');
        Route::put('order-view/{id}/detail', 'Frontend\OrderController@changeStatus')
            ->name('frontend.order.change');
        Route::get('checkout/success', 'Frontend\OrderController@orderSuccess')
            ->name('frontend.order.success');
        Route::get('user/order-list', 'Frontend\OrderController@orderList')
            ->name('frontend.order.list');
        Route::post('/order/resend/email', 'Frontend\OrderController@resendEmail');
    });

    Route::get('/user/{user_id}/{user_name}', 'Frontend\UserProfilesController@profileDiffUser')
        ->name('frontend.user.different.profiles');

    Route::get('/partner/list-all', 'Frontend\HomeController@partnerList')->name('frontend.partner.list');
    Route::get('/partner/json/getList', 'Frontend\HomeController@jsonPartnerList');

    Route::post('/detail/{id}/review', 'Frontend\DetailProductsController@reviewProduct');

    Route::get('/contact/sendemail', 'Frontend\HomeController@indexSendEmail')->name('frontend.contact.index');
    Route::post('/contact/sendemail', 'Frontend\HomeController@sendEmail')->name('frontend.sendemail');

    // Comment
    Route::get('/comment/json/getList', 'Frontend\DetailProductsController@jsonCommentList');

    // Detail product
    Route::get('detail/{id}/{name}', 'Frontend\DetailProductsController@index')->name('detail');

    Route::get('/{parent}', 'Frontend\ProductsListController@showParentCategories')->name('nav');
    Route::get('/{parentLink}/{subLink}', 'Frontend\ProductsListController@showSubCategory')->name('sub_Nav');
});
